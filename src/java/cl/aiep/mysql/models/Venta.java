
package cl.aiep.mysql.models;

import cl.aiep.mysql.conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Venta {
    private int id;
    private int folio;
    private String fecha;
    private String tipo;

    public Venta(int id, int folio, String fecha, String tipo) {
        this.id = id;
        this.folio = folio;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public Venta() {
    }

    public int getId() {
        return id;
    }

    public int getFolio() {
        return folio;
    }

    public String getFecha() {
        return fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
   
 
}
