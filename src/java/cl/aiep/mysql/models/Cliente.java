
package cl.aiep.mysql.models;

import cl.aiep.mysql.conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Cliente {
    private int Rut;
    private String nombre;
    private String apellido;
   

    public Cliente(int Rut, String nombre, String apellido) {
        this.Rut = Rut;
        this.nombre = nombre;
        this.apellido = apellido;
       
    }

    public Cliente() {
    }

    public int getRut() {
        return Rut;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

  

    public void setRut(int Rut) {
        this.Rut = Rut;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


    
        public static ArrayList<Cliente> listar() throws SQLException
    {
        
        try{
        
            String Query="select CliRut,CliNombre,CliApellido from Cliente";
            Conexion cnx = new Conexion();
            cnx.getConnection();
            ResultSet resultado=cnx.devolverDatos(Query);
            
             ArrayList<Cliente>lista=new ArrayList<>();
    
                           
            while(resultado.next()){  
                lista.add(
                        new Cliente(
                                    resultado.getInt("CliRut"), 
                                    resultado.getString("CliNombre"),
                                    resultado.getString("CliApellido")
                        )
                );
            
            
        
            }
        return lista;
            
        }catch (SQLException ex){
                return null; 
                }   
            
              }
}
