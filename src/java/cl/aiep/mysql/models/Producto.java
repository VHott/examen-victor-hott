
package cl.aiep.mysql.models;

import cl.aiep.mysql.conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Producto {
    private int codigo;
    private String nombre;
    private String marca;

    public Producto(int codigo, String nombre, String marca) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.marca=marca;
    }

    public Producto() {
    }

    public int getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
           public static ArrayList<Producto> listar() throws SQLException
    {
        
        try{
        
            String Query="select ProCodigo,ProNombre,ProMarca from Producto";
            Conexion cnx = new Conexion();
            cnx.getConnection();
            ResultSet resultado=cnx.devolverDatos(Query);
            
             ArrayList<Producto>lista=new ArrayList<>();
    
                           
            while(resultado.next()){  
                lista.add(
                        new Producto(
                                    resultado.getInt("ProCodigo"), 
                                    resultado.getString("ProNombre"),
                                    resultado.getString("ProMarca")
                                    
                        )
                );
            
            
        
            }
        return lista;
            
        }catch (SQLException ex){
                return null; 
                }   
            
              }
}
