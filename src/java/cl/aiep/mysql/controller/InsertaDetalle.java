/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.aiep.mysql.controller;

import cl.aiep.mysql.conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Victor Hott
 */
public class InsertaDetalle extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertaDetalle</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertaDetalle at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            String codprod= request.getParameter("producto");
            String cantidad     = request.getParameter("cantidad");
            String descuento  = request.getParameter("descuento");
            String idventa=request.getParameter("IdVenta");
            int precio=0;
            int precioreal=0;
            int precioventa=0;
           
            Conexion cnx = new Conexion();
            
            boolean guardar=false;
           
        try {
            cnx.getConnection();
             String  query = "SELECT ProPrecio FROM Producto WHERE ProCodigo='"+codprod+"'";
            
                 ResultSet listado = cnx.devolverDatos(query);
                 while(listado.next()){
                 precio = listado.getInt("ProPrecio") ;
                 }
                 
                 precioreal=precio*Integer.parseInt(cantidad);
                 precioventa=precioreal-(precioreal*Integer.parseInt(descuento))/100;
                 
                        query = "INSERT INTO Detalle_Venta "+
                        "(ProCodigo, VenCantidad, VenPrecioReal ,VenDescuento, VenPrecioVenta,IdVenta) "+
                        "values "+
                        "( '"+codprod+"', '"+cantidad+"', '"+precioreal+"','"+descuento+"', "+precioventa+",'"+idventa+"')";
                        
            guardar = cnx.guardarDatos( query );
            response.sendRedirect("DetalleVenta.jsp?IdVenta="+idventa);
              
            
        } catch (SQLException ex) {
            Logger.getLogger(InsertaDetalle.class.getName()).log(Level.SEVERE, null, ex);
             
        } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
