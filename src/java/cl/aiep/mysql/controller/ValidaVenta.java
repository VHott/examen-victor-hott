/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.aiep.mysql.controller;

import cl.aiep.mysql.conexion.Conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Victor Hott
 */
public class ValidaVenta extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ValidaVenta</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ValidaVenta at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             Conexion cnx = new Conexion();
                String folio = request.getParameter("folio");
                String tipoventa = request.getParameter("tipoventa");
                String fecha = request.getParameter("fecha");
                String rut = request.getParameter("cliente");
              String id="";
                 boolean guardar = false;
                 if(Integer.parseInt(tipoventa)==1){
                     tipoventa="Boleta";
                 }
                 else{
                     tipoventa="Factura";}
                 
                
         
            try {
                cnx.getConnection(); 
                String  query = "SELECT Id FROM Venta WHERE VenFolio='"+folio+"' AND VenTipo ='"+tipoventa+"'";
            
                 ResultSet listado = cnx.devolverDatos( query );
                 while(listado.next()){
                 id = String.valueOf( listado.getInt("Id") );
                
                 }

                    if(id.equals("")){
                         query= "INSERT INTO Venta "+
                                "(VenFolio, VenFecha, VenTipo ,CliRut) "+
                                "values "+
                                "( '"+folio+"', '"+fecha+"', '"+tipoventa+"','"+rut+"')";
                        guardar = cnx.guardarDatos(query);
                        
                        query = "select Id from Venta order by id desc limit 1";
                         ResultSet idventa = cnx.devolverDatos( query );
                         
                 while(idventa.next()){
                 id = String.valueOf( idventa.getInt("Id") );
                
                 }
                       response.sendRedirect("DetalleVenta.jsp?IdVenta="+id);
                    } 
                    else{
                        
                        response.sendRedirect("IngresoVentas.jsp?error");
                        
                         
                    }
            } catch (SQLException ex) {
            Logger.getLogger(ValidaVenta.class.getName()).log(Level.SEVERE, null, ex);
            }           
            
       
        } 
   
    
   

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

