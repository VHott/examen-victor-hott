
package cl.aiep.mysql.conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Conexion {
        private String server;
        private String user;
        private String base;
        private String clave;
        private int port;
        private String url;
        private Connection conexion;
    
    public Conexion(){//constructor
        this.clave  = "4558350";
        this.server = "localhost";
        this.user   = "root";
        this.port   = 3306;
        this.base   = "Examen";
        
        //construimos string de conexion para conectarse a la base de datos  
        this.url  = "jdbc:mysql://" + this.server + ":" + this.port + "/";
        this.url += this.base + "?characterEncoding=latin1";
       // "jdbc:mysql://LOCALHOST:3306/Ejemplo?characterEncoding=latin1"
    }
 
   
    public void getConnection() throws SQLException{
        this.conexion = null; 
        
       try{
           //decirle al objeto Connection del JDK que trabaje o que 
           //realice encapsulamiento a partir de esta clase com.microsoft.sqlserver.jdbc.SQLServerDriver
           Class.forName("com.mysql.jdbc.Driver");
           //aca se creae la cponexion
           this.conexion = (Connection) DriverManager.getConnection(  
                   this.url,  
                   this.user, 
                   this.clave 
           );
           //para testint escribirmos en la consila exito
           System.out.println(" exito al conectarse");
       }catch(ClassNotFoundException ex){
           //rn caso de error escribimos en la consola error
            System.out.println("Error de conexion : " + ex.getMessage());
       }   
    }
         public ResultSet devolverDatos( String query ) throws SQLException{
    
        return this.conexion.createStatement().executeQuery( query );
        
    
    }
        public boolean guardarDatos( String query ) throws SQLException{
        return this.conexion.createStatement().execute(query);
     
    }
         public boolean ValidarDatos( String query) throws SQLException{
        return this.conexion.createStatement().execute(query );
    }
          public void EliminarDatos( String query) throws SQLException{
        this.conexion.createStatement().executeUpdate( query );
    }
         
}