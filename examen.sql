Create database Examen
use Examen

create table Cliente (
CliRut INT PRIMARY KEY,
CliNombre VARCHAR(50),
CliApellido VARCHAR(50),
CliFechaIngreso DATE,
CliDireccion VARCHAR(50),
CliGiro VARCHAR(20),
CliFono INT
)
;
CREATE table Venta (
Id INT PRIMARY KEY auto_increment,
VenFolio INT,
VenFecha DATE,
VenTipo VARCHAR(30),
CliRut INT,
FOREIGN KEY (CliRut) 
REFERENCES Cliente(CliRut)
)
;

create table Producto (
ProCodigo INT PRIMARY KEY auto_increment,
ProCodigoBarra INT,
ProNombre VARCHAR(50),
ProMarca VARCHAR(50),
ProPrecio INT
)
;
create table Detalle_Venta (
Id INT PRIMARY KEY AUTO_INCREMENT,
ProCodigo INT,
VenCantidad INT,
VenPrecioReal INT,
VenDescuento INT,
VenPrecioVenta INT,
IdVenta INT,
foreign key (ProCodigo)
references Producto(ProCodigo),
foreign key (IdVenta)
references Venta(Id)
)
;

create table Usuario (
Id INT PRIMARY KEY AUTO_INCREMENT,
Nombre VARCHAR(50),
Correo VARCHAR(50),
Clave VARCHAR(50)
)
;


insert into Usuario (Nombre,Correo,Clave) values("Victor Hott","victor.hott@clinicale.cl","1234");
select * from usuario

insert into Cliente
(CliRut,CliNombre,CliApellido,CliFechaIngreso,CliDireccion,CliGiro,CliFono)
Values(165879457,"Paola","Hott",20/07/2019,"Ernesto Riquelme 315","Ferreteria",74515465)

insert into Producto (ProCodigoBarra,ProNombre,ProMarca,ProPrecio)
Values(100001,"Arroz","Miraflores",1000),(100002,"Arroz","Tucapel",900),(100003,"Fideos","Luchetti",450),
(100004,"Martillo","Sodimac",3500),(100005,"Alicate","Easy",2000),(100006,"Serrucho","Construmart",5000)

select * from producto

select CliRut, concat(CliNombre," ",CliApellido) from Cliente

select * from venta
select * from Detalle_Venta

SELECT
	Venta.Id,
	Venta.VenFolio,
	Venta.VenTipo,
	Venta.VenFecha, 
	Cliente.CliRut, 
    Detalle_Venta.VenPrecioVenta,
	concat(Cliente.CliNombre,Cliente.CliApellido) 
	
FROM  Venta 
INNER JOIN Cliente ON Cliente.CliRut = Venta.CliRut 
INNER JOIN Detalle_Venta ON Detalle_Venta.IdVenta = Venta.Id

                        

Insert into Detalle_Venta (ProCodigo,VenCantidad,VenPrecioReal,VenDescuento,VenPrecioVenta,IdVenta)
Values(4,1,3500,10,3150,17)

SELECT 
                                Detalle_Venta.VenCantidad,
                                Producto.ProNombre,
                                Producto.ProPrecio,
                                Detalle_Venta.VenPrecioReal
                        FROM  Detalle_Venta 
                        INNER JOIN Producto ON Producto.ProCodigo = Detalle_Venta.ProCodigo 
	delete  from Venta where Id <5000