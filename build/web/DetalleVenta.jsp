<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.mysql.conexion.Conexion"%>
<%@page import="cl.aiep.mysql.models.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Conexion cnx =  new Conexion();
    cnx.getConnection();
     
    
    
      
               String query = "SELECT " +
                                "Detalle_Venta.VenCantidad," +
                                "Producto.ProNombre," +
                                "Producto.ProPrecio," + 
                                "Detalle_Venta.VenPrecioVenta, " +
                                "Detalle_Venta.Id " +
                        "FROM  Detalle_Venta " +
                        "INNER JOIN Producto ON Producto.ProCodigo = Detalle_Venta.ProCodigo "+
                        "WHERE Detalle_Venta.IdVenta= "+request.getParameter("IdVenta");
                       
                       
                System.out.println( query );
               
    ResultSet tabla     =  cnx.devolverDatos(query);
 %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Detalle de Venta</title>
    </head>
    <body>
        <form action="InsertaDetalle" method="POST">
            <div class="form-row">
                <div class="form-group col-md-1">
                  <label for="cantidad">Cantidad</label>
                  <input type="text" class="form-control" id="cantidad" name="cantidad" >
                </div>
                <div class="form-group col-md-5">
                  <label for="producto">Producto</label>
                  <select class="form-control" id="producto" name="producto">
                      <option value="0"> Seleccionar</option> 
                         <% 

                    for (Producto p: Producto.listar()){%>
                                                                               
                    <option value="<%= p.getCodigo()  %>"><%= p.getNombre() + "-" + p.getMarca()%> 
                    
                    <%}%>
                      
                  </select>    
                </div>
                  <div class="form-group col-md-1">
                  <label for="descuento">%Desc</label>
                  <input type="text" class="form-control" id="descuento" name="descuento">
                </div>
                    <input type="hidden" value="<%=request.getParameter("IdVenta")%>" name="IdVenta">
                <div class="form-group col-md-1 mt-4">
                 <button type="submit" class="btn btn-success" name="Agregar">Agregar</button>
                 
                   
                </div>
              </div>
        </form>
                <form action="EliminarDetalle" method="POST" id="formquitar"> 
                   <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Unitario</th>
                        <th>Total</th>
                        <th>Quitar</th>
                       
                    </tr>
                </thead>
                      <%
                      double suma=0;
                      double iva=0;
                      double total=0;
                      double neto=0;
                      
                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        out.println( "<td>"); 
                        out.println("<input type='button' value='Quitar'  class='btn btn-primary' onclick='pregunta( " + tabla.getInt( 5 ) + " )'>");
                        out.println("</tr>");
                        suma=suma+Integer.parseInt(tabla.getString(4));
                    }
                    iva=(suma*0.19);
                    neto=suma-iva;
                    total=(neto+iva);
                   
                    %>
            
            </table>
            
            </form>
                <form action="IngresoVentas.jsp" method="GET"> 
                    <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="neto">Valor Neto</label>
                    </div>
                      <div class="form-group col-md-3">
                        <input type="text" class="form-control" id="neto" name="valorneto" value="<%=neto%>">
                      </div>
                    </div>
                      <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="iva">I.V.A (19%)</label>
                    </div>
                      <div class="form-group col-md-3">
                        <input type="text" class="form-control" id="iva" name="valoriva" value="<%=iva%>">
                      </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-3">
                      <label for="total">Valor Total)</label>
                    </div>
                      <div class="form-group col-md-3">
                        <input type="text" class="form-control" id="total" name="valortotal"  value="<%=total%>">
                      </div>
                        <div class="form-group col-md-3">
                        <input type="submit" class="btn btn-success" name="valortotal" value="Finalizar" >
                      </div>
                    </div>
                </form>
                <form action="EliminarDetalle" method="POST" id="formEliminar">
                 <input type="hidden" id="IdDetalleVenta" name="IdDetalleVenta">
    </form>
                  <script>
    
            function pregunta(id){
                if (confirm('¿Estas seguro que desea Eliminar este producto del Detalle?')){
                 document.getElementById("formquitar").submit()
                 document.getElementById("IdDetalleVenta").value = id;
                 document.getElementById("formEliminar").submit();
                 
            }
            
        }

    </script>
    </body>
</html>
