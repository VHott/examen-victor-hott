package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import cl.aiep.mysql.conexion.Conexion;
import cl.aiep.mysql.models.Cliente;

public final class IngresoVentas_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    Conexion cnx =  new Conexion();
    cnx.getConnection();
     
    
    
      
               String query = "SELECT " +
                                "Venta.VenFolio," +
                                "Venta.VenTipo," +
                                "Venta.VenFecha," + 
                                "Cliente.CliRut, " +
                                "concat(Cliente.CliNombre,Cliente.CliApellido), " +
                                "sum(Detalle_Venta.VenPrecioVenta) " +
                        "FROM  Venta " +
                        "INNER JOIN Cliente ON Cliente.CliRut = Venta.CliRut "+
                        "INNER JOIN Detalle_Venta ON Detalle_Venta.IdVenta = Venta.Id ";
                        
                        
                       
                       
                System.out.println( query );
               
    ResultSet tabla     =  cnx.devolverDatos(query);
 
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\n");
      out.write("\n");
      out.write("        <title>Ingreso de Venta</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form action=\"ValidaVenta\" method=\"POST\">\n");
      out.write("              <div class=\"form-row\">\n");
      out.write("                <div class=\"form-group col-md-2\">\n");
      out.write("                  <label for=\"folio\">Folio</label>\n");
      out.write("                  <input type=\"text\" class=\"form-control\" id=\"folio\" name=\"folio\" >\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group col-md-4\">\n");
      out.write("                  <label for=\"tipoventa\">Tipo de Venta</label>\n");
      out.write("                  <select class=\"form-control\" id=\"tipoventa\" name=\"tipoventa\">\n");
      out.write("                      <option value=\"0\"> Seleccionar</option> \n");
      out.write("                      <option value=\"1\"> Boleta</option>\n");
      out.write("                      <option value=\"2\"> Factura</option>\n");
      out.write("                  </select>    \n");
      out.write("                </div>\n");
      out.write("                  <div class=\"form-group col-md-4\">\n");
      out.write("                  <label for=\"fecha\">Fecha Venta</label>\n");
      out.write("                  <input type=\"date\" class=\"form-control\" id=\"fecha\" name=\"fecha\">\n");
      out.write("                </div>\n");
      out.write("              </div>\n");
      out.write("            <div class=\"form-row\">\n");
      out.write("                <div class=\"form-group col-md-4\">\n");
      out.write("                  <label for=\"cliente\">Nombre</label>\n");
      out.write("                 <select class=\"form-control\" id=\"cliente\" name=\"cliente\">\n");
      out.write("                     <option value=\"0\"> Seleccionar</option>\n");
      out.write("                     ");
 

                    for (Cliente c: Cliente.listar()){
      out.write("\n");
      out.write("                                                                               \n");
      out.write("                    <option value=\"");
      out.print( c.getRut()  );
      out.write('"');
      out.write('>');
      out.print( c.getNombre() + " " + c.getApellido());
      out.write(" \n");
      out.write("                    \n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                 </select>   \n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group col-md-4 mt-4\">\n");
      out.write("                 <button type=\"submit\" class=\"btn btn-success\" name=\"CrearVenta\">Crear Venta</button>\n");
      out.write("                   \n");
      out.write("                </div>\n");
      out.write("            </div>  \n");
      out.write("        </form>\n");
      out.write("                      <table border=\"1\" class=\"table table-hover\" style=\"width: 100%\">\n");
      out.write("                <thead>\n");
      out.write("                    <tr>\n");
      out.write("                        <th>Folio</th>\n");
      out.write("                        <th>Tipo Venta</th>\n");
      out.write("                        <th>Fecha</th>\n");
      out.write("                        <th>Rut Cliente</th>\n");
      out.write("                        <th>Nombre Cliente</th>\n");
      out.write("                        <th>Monto Total</th>\n");
      out.write("                       \n");
      out.write("                    </tr>\n");
      out.write("                </thead>\n");
      out.write("                      ");

                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        out.println("<td>" + tabla.getString(5) + "</td>");
                        out.println("<td>" + tabla.getString(6) + "</td>"); 
                        out.println("</tr>");
                    }
                
      out.write("\n");
      out.write("            \n");
      out.write("            </table>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
