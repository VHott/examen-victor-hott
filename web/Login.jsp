<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="CSS/Login.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Inicio de Sesión</title>
    </head>
    <body>
        <header>
        <form action="ValidaUsuario" method="POST">
            <h1>Autentificación de Usuario</h1>
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="usuario">Usuario</label>
                  <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Ingrese su Email">
                </div>
            </div>
                 <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="clave">Contraseña</label>
                  <input type="password" class="form-control" id="clave" name="clave" placeholder="Ingrese su Contraseña">
                </div>
            </div>
                             <% 
          if(  request.getParameter("error") != null) { 

          String clase = (request.getParameter("error")!=null)?"danger":"warning";
          
      %>
      <div class="alert alert-<%=clase%> alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-warning"></i> Error de Acceso!</h4>
              
            </div>  
        <%
        }
        %>
                <button type="submit" class="btn btn-primary">Ingresar</button>
             
         </form>
        </header>
    </body>
</html>
