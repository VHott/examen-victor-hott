<%@page import="java.sql.ResultSet"%>
<%@page import="cl.aiep.mysql.conexion.Conexion"%>
<%@page import="cl.aiep.mysql.models.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Conexion cnx =  new Conexion();
    cnx.getConnection();
     
    
    
      
               String query = "SELECT " +
                                "Venta.VenFolio," +
                                "Venta.VenTipo," +
                                "Venta.VenFecha," + 
                                "Cliente.CliRut, " +
                                "Detalle_Venta.VenPrecioVenta, " +
                                "concat(Cliente.CliNombre,' ',Cliente.CliApellido) " +   
                        "FROM  Venta " +
                        "INNER JOIN Cliente ON Cliente.CliRut = Venta.CliRut "+
                        "INNER JOIN Detalle_Venta ON Detalle_Venta.IdVenta = Venta.Id ";
                        
                        
                       
                       
                System.out.println( query );
               
    ResultSet tabla     =  cnx.devolverDatos(query);
 %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Ingreso de Venta</title>
    </head>
    <body>
        <form action="ValidaVenta" method="POST">
              <div class="form-row">
                <div class="form-group col-md-2">
                  <label for="folio">Folio</label>
                  <input type="text" class="form-control" id="folio" name="folio" >
                </div>
                <div class="form-group col-md-4">
                  <label for="tipoventa">Tipo de Venta</label>
                  <select class="form-control" id="tipoventa" name="tipoventa">
                      <option value="0"> Seleccionar</option> 
                      <option value="1"> Boleta</option>
                      <option value="2"> Factura</option>
                  </select>    
                </div>
                  <div class="form-group col-md-4">
                  <label for="fecha">Fecha Venta</label>
                  <input type="date" class="form-control" id="fecha" name="fecha">
                </div>
              </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="cliente">Nombre</label>
                 <select class="form-control" id="cliente" name="cliente">
                     <option value="0"> Seleccionar</option>
                     <% 

                    for (Cliente c: Cliente.listar()){%>
                                                                               
                    <option value="<%= c.getRut()  %>"><%= c.getNombre() + " " + c.getApellido()%> 
                    
                    <%}%>
                 </select>   
                </div>
                <div class="form-group col-md-4 mt-4">
                 <button type="submit" class="btn btn-success" name="CrearVenta">Crear Venta</button>
                   
                </div>
            </div>  
                          
                             <% 
          if(  request.getParameter("error") != null) { 

          String clase = (request.getParameter("error")!=null)?"danger":"warning";
          
      %>
      <div class="alert alert-<%=clase%> alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-warning"></i> ERROR! La venta ya EXISTE!</h4>
              
            </div>  
        <%
        }
        %>
        </form>
                      <table border="1" class="table table-hover" style="width: 100%">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Tipo Venta</th>
                        <th>Fecha</th>
                        <th>Rut Cliente</th>
                        <th>Nombre Cliente</th>
                        <th>Monto Total</th>
                       
                    </tr>
                </thead>
                      <%
                    while (tabla.next()) {
                        out.println("<tr>");
                        out.println("<td>" + tabla.getString(1) + "</td>");
                        out.println("<td>" + tabla.getString(2) + "</td> ");
                        out.println("<td>" + tabla.getString(3) + "</td>");
                        out.println("<td>" + tabla.getString(4) + "</td>");
                        out.println("<td>" + tabla.getString(6) + "</td>");
                        out.println("<td>" + tabla.getString(5) + "</td>"); 
                        out.println("</tr>");
                    }
                %>
            
            </table>
    </body>
</html>
